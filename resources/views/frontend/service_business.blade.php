<!DOCTYPE html>
<html lang="en">
<head>

	<!-- SITE TITTLE -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Future Track IT</title>

	<!-- PLUGINS CSS STYLE -->
	<link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="plugins/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css"> -->
	<link href="plugins/selectbox/select_option1.css" rel="stylesheet">
	<link href="plugins/slick/slick.css" rel="stylesheet" media="screen">
	<link href="plugins/slick/slick-theme.css" rel="stylesheet" media="screen">
	<link href="plugins/prismjs/prism.css" rel="stylesheet">
	<link rel="stylesheet" href="plugins/fancybox/jquery.fancybox.min.css" />
	<link href="plugins/selectbox/select_option1.css" rel="stylesheet">
	<link href="plugins/isotope/isotope.min.css" rel="stylesheet">
	<link href="plugins/animate.css" rel="stylesheet">

	<!-- REVOLUTION SLIDER -->
	<link rel="stylesheet" href="plugins/revolution/css/settings.css">
	<link rel="stylesheet" href="plugins/revolution/css/layers.css">
	<link rel="stylesheet" href="plugins/revolution/css/navigation.css">
	
	<!-- CUSTOM CSS -->
	<link href="css/tooltip.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/default.css" id="option_color">
	<!-- FAVICON -->
	<link href="img/favicon.png" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body id="body" class="portfolio-project-details">
	<!-- Preloader -->
	<div id="preloader" class="smooth-loader-wrapper">
		<div class="smooth-loader">
			<div class="loader1">
			<div class="loader-target">
				<div class="loader-target-main"></div>
				<div class="loader-target-inner"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- HEADER -->
	<header id="pageTop" class="header">
	
		<!-- NAVBAR -->
		<nav class="navbar navbar-expand-md main-nav navbar-sticky header-transparent">
			<div class="container">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="burger-menu icon-toggle"><i class="icon-menu icons"></i></i></span>
				</button>
				<a class="navbar-brand" href="index.html">
					<img src="img/logo.png"/>
				</a>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item ">
							<a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
						</li>
						
						<li class="nav-item ">
									<a class="nav-link" href="portfolio-fullwidth.html">Portfolio</a>
								</li>

								<li class="nav-item dropdown drop_single ">
									<a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">News & Updates</a>
									<ul class="dropdown-menu dd_first">
										<li><a href="#">FTIT Market Place</a></li>
										<li><a href="blog.html">Official Blogs</a></li>
									</ul>
								</li>

								<!-- Problems -->

								<li class="nav-item dropdown drop_single ">
									<a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="#">Services</a>
									<ul class="dropdown-menu dd_first">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Web Development</a>
											<ul class="dropdown-menu submenu">
												<li><a href="service_responsive_webdesign.html">Responsive WebDesign</a></li>
												<li><a href="service_e-commerce.html">E-Commerce Websites</a></li>
												<li><a href="service_content_management.html">Content MGMT SYS(CMS)</a></li>
												<li><a href="service_event_booking.html">Event Booking System</a></li>
												<li><a href="service_website_repairs.html">Website Repairs & MTC</a></li>
											</ul>
										</li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Software Development</a>
											<ul class="dropdown-menu submenu">
												<li><a href="service_erp.html">ERP SW Development</a></li>
												<li><a href="service_accounting.html">Accounting SW DEV</a></li>
												<li><a href="service_management.html">Management SW DEV</a></li>
												<li><a href="service_automation.html">Automation SW DEV</a></li>
												<li><a href="service_antivirus.html">Anti-virus SW DEV</a></li>
												<li><a href="service_bpo.html">BPO SW Development</a></li>
											</ul>
										</li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Apps Development</a>
											<ul class="dropdown-menu submenu">
												<li><a href="service_ios.html">IOS App Development</a></li>
												<li><a href="service_android.html">Android App Development</a></li>
											</ul>
										</li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Digital Marketing</a>
											<ul class="dropdown-menu submenu">
												<li><a href="service_email.html">Email Newsletter</a></li>
												<li><a href="service_networking.html">Social Networking</a></li>
												<li><a href="service_sem.html">Search Engine MKTG(SEM)</a></li>
												<li><a href="service_seo.html">Search Engine OP(SEO)</a></li>
												<li><a href="service_on_off.html">On-site/Off-site MKTG</a></li>
											</ul>
										</li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Web Hosting</a>
											<ul class="dropdown-menu submenu">
												<li><a href="service_shared.html">Shared Web Hosting</a></li>
												<li><a href="service_dedicated.html">Dedicated Web Server</a></li>
												<li><a href="service_reseller.html">Reseller Web Hosting</a></li>
												<li><a href="service_cloud.html">Cloud Web Hosting</a></li>
												<li><a href="service_virtual.html">Virtual PVT SRV(VPS)</a></li>
												<li><a href="service_colocation.html">Collocation Web Hosting </a></li>
											</ul>
										</li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Graphics Design</a>
											<ul class="dropdown-menu submenu">
												<li><a href="service_logo.html">Logo & identity</a></li>
												<li><a href="service_web.html">Web & app design</a></li>
												<li><a href="service_business.html">Business & advertising</a></li>
												<li><a href="service_clothing.html">Clothing & merchandise</a></li>
												<li><a href="service_art.html">Art & illustration</a></li>
												<li><a href="service_packaging.html">Packaging & label</a></li>
											</ul>
										</li>
									</ul>							
								</li>

								<!-- End Problems -->

								<li class="nav-item dropdown drop_single ">
									<a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">About Us</a>
									<ul class="dropdown-menu dd_first">
										<li><a href="page-aboutus.html">About Us</a></li>
										<li><a href="page-faq.html">FAQ</a></li>
									</ul>
								</li>
								
								<li class="nav-item">
									<a class="nav-link" href="page-contactus.html">Contact Us</a>
								</li>
						<!-- header search -->
						<li class="nav-item search_hook">
							<a href="javascript:void(0)" data-toggle="modal" data-target="#searchModal" class="btn-search nav-link"><i class="fa fa-search"></i></a>
							<form  class="search_form">
								<input type="text" name="search" placeholder="Search...">
								<button class="no-bg btn-search" type="submit"><i class="fa fa-search"></i></button>
							</form>
						</li>
					</ul>
				</div>
				<!-- header search ends-->
			</div>
		</nav>
	</header>
	<div class="main-wrapper ">

<!-- BREDCRUMB -->
<div class="bredcrumb bg-image text-center"  style="background-image: url('img/bredcrumb.jpg');">
	<div class="row bredcrumb-inner">
		<div class="col-sm-12  align-self-center">
			<h2>Graphics Design</h2>
			<ul class="">
				<li><a href="" class="bread_link">Home</a></li>
				<li>Business & advertising</li>
			</ul>
		</div>
	</div>
</div>

<section class="project-details-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-7">
				<div class="image_carousel arrow-shade">
					<div id="project-carousel" class="element_row">
						<div class="image_single">
							<div class="card_img">
								<img class="img-full" src="img/portfolio/pd1.jpg" alt="Card image">
							</div>
						</div>
						<div class="image_single">
							<div class="card_img">
							<img class="img-full" src="img/portfolio/pd2.jpg" alt="Card image">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-4 sidebar">
				<ul class="list-group">
					<li class="list-group-item"><a href="#">Other Services</a></li>
					<li class="list-group-item"><a href="service_logo.html">Logo & identity</a></li>
					<li class="list-group-item"><a href="service_web.html">Web & app design</a></li>
					<li class="list-group-item"><a href="service_clothing.html">Clothing & merchandise</a></li>
					<li class="list-group-item"><a href="service_art.html">Art & illustration</a></li>
					<li class="list-group-item"><a href="service_packaging.html">Packaging & label</a></li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="flex-wrap project-desc">
					<div class="section-title title-ex1">
						<h2 class="title-text">Project Details</h2>
					</div>
					<p class="description pb-3", align=justify>
						Sagittislectus,volutpatnecessitatibus praesentium vestibulum atque quidem quaerat sapien. Ex dui aliqua orci dictum integer! est elit. Ipsam numquam. Accumsan explicabo vulputate quam aspernatur platea inventore iure vel ante, Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam reprehenderit a eaque quo provident nostrum inventore asperiores perspiciatis facere, facilis omnis! Harum expedita porro suscipit voluptatibus nostrum placeat magni numquam.
					</p>
					<p class="description pt-3">
						Lectus faucibus, ex eveniet! Lacus ipsum repellendus habitasse malesuada dictum dolorem purus iure congue quibusdam! Rutrum, hic itaque. Irure nihil! Ducimus?
					</p>
				</div>
			</div>
		</div>

	</div>
</section>

<!-- FOOTER -->
			<footer class="footer footer-classic bg-dark">
				<div class="container">
				<div class="row">
		<!-- About -->
		<div class="col-md-3 md-margin-bottom-40 box_down">
			<a href="index.html"><img id="logo-footer" class="footer-logo" src="img/logo.png" alt="web design company in dhaka bangladesh"></a>
					<p align=justify>FUTURE TRACK IT is one of the leading IT Company in Bangladesh, has started its journey 
					with an earnest aim to emerge as one the finest Web Development Firms in the world. 
					We started our company  <a href="page-aboutus.html"><span class="read-more">Read more</span></a><!-- read-more class in tooltip.css -->
					<br/> </p>  
					</div><!--/col-md-3-->
					<div class="col-md-3 md-margin-bottom-40">
					<div class="posts">
					<div class="foot-headline">Our Services</div>
					<ul class="list-unstyled link-list">
					<li > </i><a href="service_responsive_webdesign.html">Responsive Website Design</a></li>
					<li> <a href="service_e-commerce.html">E-Commerce Websites</a></li>
					<li> <a href="service_content_management.html">CMS Development</a></li>
					<li> <a href="service_event_booking.html">Event Booking System</a></li>
					<li> <a href="service_website_repairs.html">Website Repairs & Maintenance</a></li>
					<li> <a href="service_email.html">Email Newsletter</a></li>
					<li> <a href="service_networking.html">Social Networking</a></li>
					<li> <a href="service_sem.html">Search Engine Marketing (SEM)</a></li>
					<li> <a href="service_seo.html">Search Engine Optimization (SEO)</a></li>
				</ul>
			</div>
		</div><!--/col-md-3--> 
<!-- Link List -->
		<div class="col-md-3 md-margin-bottom-40">
			<div class="foot-headline">Our Services </div>
				<ul class="list-unstyled link-list">
				<li><a href="service_on_off.html"title="Software Development">On-site & Off-site Marketing</a></li>
				<li><a href="service_shared.html"title="Web Design Company in Bangladesh">Shared Web Hosting</a></li>
				<li><a href="service_dedicated.html"title="Web Development">Dedicated Web Server</a></li>
				<li><a href="service_reseller.html"title="E-Commerce Solutions">Reseller Web Hosting</a></li>
				<li><a href="service_cloud.html">Cloud Web Hosting</a></li>
				<li><a href="service_virtual.html"title="Domain">Virtual Private Server(VPS)</a></li>
				<li><a href="service_colocation.html"title="Hosting">Collocation Web Hosting</a> </li>
				<li><a href="service_management.html"title="Hosting">Management Software DEV</a></li>
				<li><a href="service_automation.html"title="Hosting">Office Automation SW DEV</a></li>
			</ul>
		</div><!--/col-md-3-->
<!-- End Link List -->                    

<!-- Address -->
		<div class="col-md-3 ">
			<div class="foot-headline ">Contact Us</div> 
				<ul class="list-unstyled link-list">
				
				<li><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					United Kingdom</a>
					<div class="collapse" id="collapseOne">
					<div class="collapse show tab-pane" style="color:#fff">
					<div class="collapse show tab-pane">
								Bullands Close Bovey Tracey Devon TQ13 9JF, United Kingdom.
					</div>
					</div>
					</div></li>

				<li><a data-toggle="collapse" data-parent="#accordion" href="#collapseExample1" aria-expanded="true" aria-controls="collapseOne">
					United Arab Emirates</a>
					<div class="collapse" id="collapseExample1">
					<div class="collapse show tab-pane" style="color:#fff">
					<div class="collapse show tab-pane">
								FZE Business Center-4 (8th Floor), P.O.Box 325 782 Ras Al Khaimah, United Arab Emirates.<br>
								Phone : +442033180427
					</div>
					</div></li>

				<li><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
					UK</a>
					<div class="collapse" id="collapseOne1">
					<div class="collapse show tab-pane">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
					</div>
					</div></li>

				<li><a data-toggle="collapse" data-parent="#accordion" href="#collapseExample11" aria-expanded="true" aria-controls="collapseOne">
					Dubai</a>
					<div class="collapse" id="collapseExample11">
					<div class="collapse show tab-pane">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
					</div>
					</div></li>

				<li><a data-toggle="collapse" data-parent="#accordion" href="#collapseExample12" aria-expanded="true" aria-controls="collapseOne">
					Spain</a>
					<div class="collapse" id="collapseExample12">
					<div class="collapse show tab-pane">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
					</div>
					</div></li>

				<li><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne3" aria-expanded="true" aria-controls="collapseOne">
					China</a>

					<div class="collapse" id="collapseOne3">
							<div class="collapse show tab-pane">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
					</div>
					</div></li>

				<li><a data-toggle="collapse" data-parent="#accordion" href="#collapseExample14" aria-expanded="true" aria-controls="collapseOne">
					Korea</a>
					<div class="collapse" id="collapseExample14">
					<div class="collapse show tab-pane">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
					</div>
					</div></li>
		</ul>
	</div><!--/col-md-3-->
</div>
	<!-- COPY RIGHT -->
		<div class="copyright">
			<hr>
				<div class="row justify-content-center">
					<div class="col-md-8 col-lg-6 ">
						<div class="social-icons text-center">
							<a href="#" class="btn btn-social btn-social-o facebook">
							<i class="fa fa-facebook-f"></i>
							</a>
							<a href="#" class="btn btn-social btn-social-o twitter">
							<i class="fa fa-twitter"></i>
							</a>
							<a href="#" class="btn btn-social btn-social-o pinterest">
							<i class="fa fa-pinterest-p"></i>
							</a>
							<a href="#" class="btn btn-social btn-social-o google-plus">
							<i class="fa fa-google-plus"></i>
							</a>
							<a href="#" class="btn btn-social btn-social-o linkedin">
							<i class="fa fa-linkedin"></i>
							</a>
							<a href="#" class="btn btn-social btn-social-o vimeo">
							<i class="fa fa-vimeo"></i>
							</a>
							<a href="#" class="btn btn-social btn-social-o dribbble">
							<i class="fa fa-dribbble" aria-hidden="true"></i>
							</a>
						<div class="copyRight_text text-center">
					<p> © Copyright 2018 Future Track IT All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</footer>
	<a href="#pageTop" class="backToTop"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- Modal -->
<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span class="bar-1"></span>
				<span class="bar-2"></span>
		</button>
		<form action="">
			<input type="text" autofocus name="search" placeholder="Search...">
		</form>
	</div>
	</div>
</div>

<!-- JAVASCRIPTS -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsQdSlW4vj5RvXp2_pLnv1s1ErfxjM5_o"></script>
<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/jquery/jquery-migrate-3.0.0.min.js"></script>
<script src="plugins/bootstrap/js/tether.min.js"></script>
<script src="plugins/bootstrap/js/popper.min.js" ></script>
<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/selectbox/jquery.selectbox-0.1.3.min.js"></script>
<script src="plugins/slick/slick.min.js"></script>
<script src="plugins/circle-progress/jquery.appear.js"></script>
<script src="plugins/isotope/isotope.min.js"></script>
<script src="plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="plugins/counterUp/waypoint.js"></script>
<script src="plugins/counterUp/jquery.counterup.js"></script>
<script src="plugins/smoothscroll/SmoothScroll.js"></script>

<script src="plugins/syotimer/jquery.syotimer.min.js"></script>

<script type="text/javascript" src="plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>


<script src="js/custom.js"></script>

</body>

</html>

