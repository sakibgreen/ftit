<?php

Route::get('/', function () {
    return view('frontend.index');
});

Auth::routes();


Route::group([ 'prefix' => 'admin'], function(){

	Route::get('/index', 'AdminController@index')->name('admin.index');	

	Route::resource('users', 'UserController');

	Route::resource('roles', 'RoleController');

	Route::resource('permissions', 'PermissionController');

	Route::resource('posts', 'PostController');

});



 