<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Panel | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  @include('admin.layouts.includes.css')
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.leftnav')
      @yield('content')
    @include('admin.layouts.footer') 
    <div class="control-sidebar-bg"></div>
  </div>
  @include('admin.layouts.includes.js')
</body>
</html>
