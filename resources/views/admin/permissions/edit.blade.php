@extends('admin.layouts.master')

@section('title', '| Edit Permission')

@section('content')
<div class="content-wrapper">

<div class='col-lg-4 col-lg-offset-4'>

    <h1><i class='fa fa-key'></i> Edit {{$permission->name}}</h1>
    <br>
    <form class="form-horizontal mt-5" action="{{route('permissions.update', $permission->id)}}" method="post" enctype="multipart/form-data">
        {{method_field('PUT')}}
        {{csrf_field()}}
        {{-- Form model binding to automatically populate our fields with permission data --}}

        <div class="form-group">
            <label for="name">Permission Name</label>
            <input type="text" name="name" class="form-control" value="{{ $permission->name}}">
        </div>
        <br>
        <button class="btn btn-primary">Edit</button>
    </form>
</div>
</div>
@endsection