<!DOCTYPE html>
<html lang="en">
<head>

	<!-- SITE TITTLE -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Future Track IT</title>

	<!-- PLUGINS CSS STYLE -->
	<link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="plugins/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
	 <!-- owl.carousel css -->
	<link href="css/owl.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css"> -->
	<link href="plugins/selectbox/select_option1.css" rel="stylesheet">
	<link href="plugins/slick/slick.css" rel="stylesheet" media="screen">
	<link href="plugins/slick/slick-theme.css" rel="stylesheet" media="screen">
	<link href="plugins/prismjs/prism.css" rel="stylesheet">
	<link rel="stylesheet" href="plugins/fancybox/jquery.fancybox.min.css" />
	<link href="plugins/selectbox/select_option1.css" rel="stylesheet">
	<link href="plugins/isotope/isotope.min.css" rel="stylesheet">
	<link href="plugins/animate.css" rel="stylesheet">

	<!-- REVOLUTION SLIDER -->
	<link rel="stylesheet" href="plugins/revolution/css/settings.css">
	<link rel="stylesheet" href="plugins/revolution/css/layers.css">
	<link rel="stylesheet" href="plugins/revolution/css/navigation.css">
	
	<!-- CUSTOM CSS -->
	<link href="css/style_2.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/sm_nav.css" rel="stylesheet">
	<link href="css/tooltip.css" rel="stylesheet">
	<link href="css/custom.css" rel="stylesheet">
	<link rel="stylesheet" href="css/default.css" id="option_color">
	<!-- News Tricker -->
	<link rel="stylesheet" href="css/marquee.css" />
	<link rel="stylesheet" href="css/example.css" />
	<!-- FAVICON -->
	<link href="img/favicon.png" rel="shortcut icon">
	
	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>

<body id="body" class="home-classic">
	
	<!-- Preloader -->
	<div id="preloader" class="smooth-loader-wrapper">
		<div class="smooth-loader">
			<div class="loader1">
			<div class="loader-target">
				<div class="loader-target-main"></div>
				<div class="loader-target-inner"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- HEADER -->
	<header id="pageTop" class="header">
	
		<!-- NAVBAR -->
		<nav class="navbar navbar-expand-md main-nav navbar-sticky header-transparent"  >
					<div class="container">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="burger-menu icon-toggle"><i class="icon-menu icons"></i></i></span>
						</button>
						<a class="navbar-brand" href="index.html">
							<img src="img/logo.png"/>
						</a>

						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav ml-auto">
								<li class="nav-item active">
									<a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
								</li>
								
								<li class="nav-item ">
									<a class="nav-link" href="portfolio-fullwidth.html">Portfolio</a>
								</li>

								<li class="nav-item dropdown drop_single ">
									<a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">News & Updates</a>
									<ul class="dropdown-menu dd_first">
										<li><a href="#">FTIT Market Place</a></li>
										<li><a href="blog.html">Official Blogs</a></li>
									</ul>
								</li>

								<!-- Problems -->

								<li class="nav-item dropdown drop_single ">
									<a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="#">Services</a>
									<ul class="dropdown-menu dd_first">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Web Development</a>
											<ul class="dropdown-menu submenu">
												<li><a href="service_responsive_webdesign.html">Responsive WebDesign</a></li>
												<li><a href="service_e-commerce.html">E-Commerce Websites</a></li>
												<li><a href="service_content_management.html">Content MGMT SYS(CMS)</a></li>
												<li><a href="service_event_booking.html">Event Booking System</a></li>
												<li><a href="service_website_repairs.html">Website Repairs & MTC</a></li>
											</ul>
										</li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Software Development</a>
											<ul class="dropdown-menu submenu">
												<li><a href="service_erp.html">ERP SW Development</a></li>
												<li><a href="service_accounting.html">Accounting SW DEV</a></li>
												<li><a href="service_management.html">Management SW DEV</a></li>
												<li><a href="service_automation.html">Automation SW DEV</a></li>
												<li><a href="service_antivirus.html">Anti-virus SW DEV</a></li>
												<li><a href="service_bpo.html">BPO SW Development</a></li>
											</ul>
										</li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Apps Development</a>
											<ul class="dropdown-menu submenu">
												<li><a href="service_ios.html">IOS App Development</a></li>
												<li><a href="service_android.html">Android App Development</a></li>
											</ul>
										</li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Digital Marketing</a>
											<ul class="dropdown-menu submenu">
												<li><a href="service_email.html">Email Newsletter</a></li>
												<li><a href="service_networking.html">Social Networking</a></li>
												<li><a href="service_sem.html">Search Engine MKTG(SEM)</a></li>
												<li><a href="service_seo.html">Search Engine OP(SEO)</a></li>
												<li><a href="service_on_off.html">On-site/Off-site MKTG</a></li>
											</ul>
										</li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Web Hosting</a>
											<ul class="dropdown-menu submenu">
												<li><a href="service_shared.html">Shared Web Hosting</a></li>
												<li><a href="service_dedicated.html">Dedicated Web Server</a></li>
												<li><a href="service_reseller.html">Reseller Web Hosting</a></li>
												<li><a href="service_cloud.html">Cloud Web Hosting</a></li>
												<li><a href="service_virtual.html">Virtual PVT SRV(VPS)</a></li>
												<li><a href="service_colocation.html">Collocation Web Hosting </a></li>
											</ul>
										</li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Graphics Design</a>
											<ul class="dropdown-menu submenu">
												<li><a href="service_logo.html">Logo & identity</a></li>
												<li><a href="service_web.html">Web & app design</a></li>
												<li><a href="service_business.html">Business & advertising</a></li>
												<li><a href="service_clothing.html">Clothing & merchandise</a></li>
												<li><a href="service_art.html">Art & illustration</a></li>
												<li><a href="service_packaging.html">Packaging & label</a></li>
											</ul>
										</li>
									</ul>							
								</li>

								<!-- End Problems -->

								<li class="nav-item dropdown drop_single ">
									<a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">About Us</a>
									<ul class="dropdown-menu dd_first">
										<li><a href="page-aboutus.html">About Us</a></li>
										<li><a href="page-faq.html">FAQ</a></li>
									</ul>
								</li>
								
								<li class="nav-item">
									<a class="nav-link" href="page-contactus.html">Contact Us</a>
								</li>
								<!-- header search -->
								<li class="nav-item search_hook">
									<a href="javascript:void(0)" data-toggle="modal" data-target="#searchModal" class="btn-search nav-link"><i class="fa fa-search"></i></a>
									<form  class="search_form">
										<input type="text" name="search" placeholder="Search...">
										<button class="no-bg btn-search" type="submit"><i class="fa fa-search"></i></button>
									</form>
								</li>
							</ul>
						</div>
						<!-- header search ends-->
					</div>
				</nav>
	
	</header>

	<div class="main-wrapper ">

<!-- BANNER SLIDER CAROUSEL -->
<section class="banner">
	<div class="rev_slider_wrapper fullwidthbanner-container">

	    <!-- the ID here will be used in the JavaScript below to initialize the slider -->
	    <div id="rev_slider_1" class="rev_slider fullwidthabanner" data-version="5.4.5" style="display:none">

	        <ul id="demo1">
	            <!-- *********************** -->
	            <!-- Slides to be added here -->
	            <!-- *********************** -->

            	<li data-transition="fade" data-slotamount="5" data-masterspeed="700" data-title="Slide 1">
		            <img src="img/slider/sl0.jpg" alt="slidebg1" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>

					<!-- LAYER NR. 1 -->
					<div class="tp-caption NotGeneric-Icon caption-1  tp-resizeme "

						data-x="['center','center','center','center']"
						data-hoffset="['0','0','0','0']"
						data-y="['middle','middle','middle','middle']"
						data-voffset="['-110','-100','-100','-100']"
						data-width="['600','500','400','300']"
						data-height="auto"
						data-fontsize="['14','14','14','14']"
						data-lineheight="['26','26','26','26']"
						data-type="text"
						data-responsive_offset="on"
						data-fontweight = "500"
						data-letterspacing = '8'
						data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":800,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
						data-textAlign="['center','center','center','center']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[0,0,0,0]"

						style="z-index: 10; white-space: inherit; text-transform:left ;cursor:default;">Welcome to Future Track IT
					</div>

		            <!-- LAYER NR. 2 -->
		            <div class="tp-caption NotGeneric-Title caption-2  tp-resizeme"

		              	data-x="['center','center','center','center']"
		              	data-hoffset="['0','0','0','0']"
		              	data-y="['middle','middle','middle','middle']"
		              	data-voffset="['-63','-55','-50','-60']"
		              	data-fontsize="['55','45','30','22']"
		              	data-lineheight="['65','50','40','30']"
		              	data-width="auto"
		              	data-height="auto"
		              	data-fontweight = "700"
		              	data-type="text"
		              	data-responsive_offset="on"

		              	data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":800,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
		              	data-textAlign="['center','center','center','center']"
		              	data-paddingtop="[10,10,10,10]"
		              	data-paddingright="[0,0,0,0]"
		              	data-paddingbottom="[10,10,10,10]"
		              	data-paddingleft="[0,0,0,0]"

		              	style="z-index: 5; white-space: nowrap;text-transform:left;">We Love Our Clients!
		            </div>

					<!-- LAYER NR. 3 -->
					<div class="tp-caption NotGeneric-SubTitle caption-3  tp-resizeme"

						data-x="['center','center','center','center']"
						data-hoffset="['0','0','0','0']"
						data-y="['middle','middle','middle','middle']"
						data-voffset="['0','10','20','0']"
						data-width="['600','500','400','300']"
						data-height="auto"
						data-fontsize="['14','14','14','14']"
						data-lineheight="['22','22','22','22']"
						data-fontweight = "300"
						data-type="text"
						data-responsive_offset="on"
						data-letterspacing = '1'
						data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":700,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
						data-textAlign="['center','center','center','center']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[0,0,0,0]"

						style="z-index: 6; font-family: 'Montserrat', sans-serif; white-space: inherit;text-transform:left;">Our commitment to you is total and we want you to always be 100% satisfied by the quality and efficiency of our service.
					</div>

					<!-- LAYER 4 -->
					<a class="tp-caption rev-btn tc-btnshadow tp-rs-menulink" href="portfolio-fullwidth.html" target="_blank" id="slide-993-layer-12"
						data-x="['center','center','center','center']"
						data-hoffset="['-88','-78','-68','0']"
						data-y="['middle','middle','middle','middle']"
						data-voffset="['100','80','100','60']"
						data-lineheight="['50','45','40','30']"
						data-width="['160','140','none','none']"
						data-height="none"
						data-whitespace="nowrap"
						data-type="button"
						data-actions=''
						data-fontsize="['14','13','12','11']"
						data-responsive_offset="off"
						data-responsive="off"
						data-frames='[{"delay":900,"speed":2000,"frame":"0","from":"x:-50px;z:0;rX:0;rY:0;rZ:0;sX:1.1;sY:1.1;skX:0;skY:0;opacity:0;fbr:100;","bgcolor":"#000000","to":"o:1;fbr:100;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","bgcolor":"#000000","to":"opacity:0;fbr:100;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"150","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;fbr:90%;","style":"c:rgba(255,255,255,1);"}]'
						data-textAlign="['center','center','center','inherit']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[20,20,20,15]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[20,20,20,15]" style="z-index: 8; min-width: 280px; max-width: 280px; white-space: nowrap; font-size: 17px; line-height: 60px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Montserrat;background-color: rgba(12,198,82,1);border-radius:4px;outline:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;"><i class="fa-icon-cube" style="font-size:17px;margin-right:5px;"></i> Our Portfolio </a>
					<!-- LAYER 5 -->
					<a class="tp-caption rev-btn rev-btn-2  tc-btnshadow" href="#" target="_blank" id="slide-993-layer-13"
						data-x="['center','center','center','center']"
						data-hoffset="['88','78','68','0']"
						data-y="['middle','middle','middle','middle']"
						data-voffset="['100','80','100','98']"
						data-lineheight="['50','45','40','30']"
						data-width="['160','140','none','none']"
						data-height="none"
						data-whitespace="nowrap"
						data-type="button"
						data-actions=''
						data-fontsize="['14','13','12','11']"
						data-responsive_offset="off"
						data-responsive="off"
						data-frames='[{"delay":900,"speed":2000,"frame":"0","from":"x:50px;z:0;rX:0;rY:0;rZ:0;sX:1.1;sY:1.1;skX:0;skY:0;opacity:0;fbr:100;","bgcolor":"#000000","to":"o:1;fbr:100;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","bgcolor":"#000000","to":"opacity:0;fbr:100;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"150","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;fbr:90%;","style":"c:rgba(255,255,255,1);"}]'
						data-textAlign="['center','center','center','inherit']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[20,20,20,15]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[20,20,20,15]" style="z-index: 9; min-width: 280px; max-width: 280px; white-space: nowrap; font-size: 17px; line-height: 60px; font-weight: 400; color: rgba(34,34,34,1); letter-spacing: px;font-family:Montserrat;background-color:rgb(255,255,255);border-radius:4px;outline:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;"><i class="fa-icon-cloud-download" style="font-size:17px;margin-right:5px;"></i> Constac Us </a>


					<!-- LAYER 6 -->

					<div class="tp-caption   tp-svg-layer tp-withaction rs-hover-ready tp-scrollbelowslider" id="slide-972-layer-4"
						data-x="['center','center','center','center']"
						data-hoffset="['0','0','0','0']"
						data-y="['bottom','bottom','bottom','bottom']"
						data-voffset="['40','20','20','20']"
						data-width="['45','45','45','45']"
						data-height="['45','45','45','45']"
						data-whitespace="nowrap"
						data-type="svg"
						data-actions="[{&quot;event&quot;:&quot;click&quot;,&quot;action&quot;:&quot;scrollbelow&quot;,&quot;offset&quot;:&quot;px&quot;,&quot;delay&quot;:&quot;&quot;,&quot;speed&quot;:&quot;1000&quot;,&quot;ease&quot;:&quot;Power1.easeInOut&quot;}]"
						data-svg_src="https://revolution.themepunch.com/wp-content/plugins/revslider/public/assets/assets/svg/hardware/ic_keyboard_arrow_down_24px.svg"
						data-svg_idle="sc:transparent;sw:0;sda:0;sdo:0;"
						data-svg_hover="sc:transparent;sw:0;sda:0;sdo:0;"
						data-basealign="slide"
						data-responsive_offset="off"
						data-responsive="off"
						data-frames="[{&quot;delay&quot;:1500,&quot;speed&quot;:1000,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:bottom;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power4.easeOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;frame&quot;:&quot;hover&quot;,&quot;speed&quot;:&quot;150&quot;,&quot;ease&quot;:&quot;Linear.easeNone&quot;,&quot;to&quot;:&quot;o:1;rX:0;rY:0;rZ:0;z:0;&quot;,&quot;style&quot;:&quot;c:rgb(46,216,163);bc:rgb(46,216,163);&quot;}]"
						data-textalign="['center','center','center','center']"
						data-paddingtop="[8,8,8,8]"
						data-paddingright="[6,6,6,6]"
						data-paddingbottom="[5,5,5,5]"
						data-paddingleft="[6,6,6,6]" style="z-index: 10; background-color: rgb(12, 198, 82); min-width: 70px; max-width: 70px; color: rgb(255, 255, 255); font-family: &quot;Open Sans&quot;; border-color: rgb(12, 198, 82); border-style: solid; border-width: 2px; border-radius: 40px; cursor: pointer; visibility: inherit; transition: none; text-align: center; line-height: 26px; margin: 0px; padding: 20px 18px 5px; letter-spacing: 0px; font-weight: 400; font-size: 16px; white-space: nowrap; min-height: 70px; max-height: 70px; opacity: 1; transform-origin: 50% 50% 0px; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); box-shadow: rgb(153, 153, 153) 0px 0px 0px 0px;" >
					</div>

	            </li>

               	<!-- MINIMUM SLIDE STRUCTURE -->
               	<li data-transition="fade">

               	    <!-- SLIDE'S MAIN BACKGROUND IMAGE -->
               	    <img src="img/slider/sl1.jpg" alt="Ocean" class="rev-slidebg">
					<!-- LAYER NR. 1 -->
					<div class="tp-caption NotGeneric-Icon caption-1  tp-resizeme "

						data-x="['center','center','center','center']"
						data-hoffset="['0','0','0','0']"
						data-y="['middle','middle','middle','middle']"
						data-voffset="['-110','-100','-100','-100']"
						data-width="['600','500','400','300']"
						data-height="auto"
						data-fontsize="['14','14','14','14']"
						data-lineheight="['26','26','26','26']"
						data-type="text"
						data-responsive_offset="on"
						data-fontweight = "500"
						data-letterspacing = '8'
						data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":800,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
						data-textAlign="['center','center','center','center']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[0,0,0,0]"

						style="z-index: 10; white-space: inherit; text-transform:left ;cursor:default;">Welcome to Future Track IT
					</div>

		            <!-- LAYER NR. 2 -->
		            <div class="tp-caption NotGeneric-Title caption-2  tp-resizeme"

		              	data-x="['center','center','center','center']"
		              	data-hoffset="['0','0','0','0']"
		              	data-y="['middle','middle','middle','middle']"
		              	data-voffset="['-63','-55','-50','-60']"
		              	data-fontsize="['55','45','30','22']"
		              	data-lineheight="['65','50','40','30']"
		              	data-width="auto"
		              	data-height="auto"
		              	data-fontweight = "700"
		              	data-type="text"
		              	data-responsive_offset="on"

		              	data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":800,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
		              	data-textAlign="['center','center','center','center']"
		              	data-paddingtop="[10,10,10,10]"
		              	data-paddingright="[0,0,0,0]"
		              	data-paddingbottom="[10,10,10,10]"
		              	data-paddingleft="[0,0,0,0]"

		              	style="z-index: 5; white-space: nowrap;text-transform:left;">We Love Our Clients!
		            </div>

					<!-- LAYER NR. 3 -->
					<div class="tp-caption NotGeneric-SubTitle caption-3  tp-resizeme"

						data-x="['center','center','center','center']"
						data-hoffset="['0','0','0','0']"
						data-y="['middle','middle','middle','middle']"
						data-voffset="['0','10','20','0']"
						data-width="['600','500','400','300']"
						data-height="auto"
						data-fontsize="['14','14','14','14']"
						data-lineheight="['22','22','22','22']"
						data-fontweight = "300"
						data-type="text"
						data-responsive_offset="on"
						data-letterspacing = '1'
						data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":700,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
						data-textAlign="['center','center','center','center']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[0,0,0,0]"

						style="z-index: 6; font-family: 'Montserrat', sans-serif; white-space: inherit;text-transform:left;">Our commitment to you is total and we want you to always be 100% satisfied by the quality and efficiency of our service.
					</div>

					<!-- LAYER 4 -->
					<a class="tp-caption rev-btn tc-btnshadow tp-rs-menulink" href="portfolio-fullwidth.html" target="_blank" id="slide-993-layer-12"
					data-x="['center','center','center','center']"
					data-hoffset="['-88','-78','-68','0']"
					data-y="['middle','middle','middle','middle']"
					data-voffset="['100','80','100','60']"
					data-lineheight="['50','45','40','30']"
					data-width="['160','140','none','none']"
					data-height="none"
					data-whitespace="nowrap"
					data-type="button"
					data-actions=''
					data-fontsize="['14','13','12','11']"
					data-responsive_offset="off"
					data-responsive="off"
					data-frames='[{"delay":900,"speed":2000,"frame":"0","from":"x:-50px;z:0;rX:0;rY:0;rZ:0;sX:1.1;sY:1.1;skX:0;skY:0;opacity:0;fbr:100;","bgcolor":"#000000","to":"o:1;fbr:100;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","bgcolor":"#000000","to":"opacity:0;fbr:100;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"150","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;fbr:90%;","style":"c:rgba(255,255,255,1);"}]'
					data-textAlign="['center','center','center','inherit']"
					data-paddingtop="[0,0,0,0]"
					data-paddingright="[20,20,20,15]"
					data-paddingbottom="[0,0,0,0]"
					data-paddingleft="[20,20,20,15]" style="z-index: 8; min-width: 280px; max-width: 280px; white-space: nowrap; font-size: 17px; line-height: 60px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Montserrat;background-color: rgba(12,198,82,1);border-radius:4px;outline:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;"><i class="fa-icon-cube" style="font-size:17px;margin-right:5px;"></i> Our Portfolio </a>
				<!-- LAYER 5 -->
				<a class="tp-caption rev-btn rev-btn-2  tc-btnshadow" href="#" target="_blank" id="slide-993-layer-13" 
					data-x="['center','center','center','center']"
					data-hoffset="['88','78','68','0']"
					data-y="['middle','middle','middle','middle']"
					data-voffset="['100','80','100','98']"
					data-lineheight="['50','45','40','30']"
					data-width="['160','140','none','none']"
					data-height="none"
					data-whitespace="nowrap"
					data-type="button"
					data-actions=''
					data-fontsize="['14','13','12','11']"
					data-responsive_offset="off"
					data-responsive="off"
					data-frames='[{"delay":900,"speed":2000,"frame":"0","from":"x:50px;z:0;rX:0;rY:0;rZ:0;sX:1.1;sY:1.1;skX:0;skY:0;opacity:0;fbr:100;","bgcolor":"#000000","to":"o:1;fbr:100;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","bgcolor":"#000000","to":"opacity:0;fbr:100;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"150","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;fbr:90%;","style":"c:rgba(255,255,255,1);"}]'
					data-textAlign="['center','center','center','inherit']"
					data-paddingtop="[0,0,0,0]"
					data-paddingright="[20,20,20,15]"
					data-paddingbottom="[0,0,0,0]"
					data-paddingleft="[20,20,20,15]" style="z-index: 9; min-width: 280px; max-width: 280px; white-space: nowrap; font-size: 17px; line-height: 60px; font-weight: 400; color: rgba(34,34,34,1); letter-spacing: px;font-family:Montserrat;background-color:rgb(255,255,255);border-radius:4px;outline:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;"><i class="fa-icon-cloud-download" style="font-size:17px;margin-right:5px;"></i> Contact Us </a>

					<!-- LAYER 6 -->

					<div class="tp-caption   tp-svg-layer tp-withaction rs-hover-ready tp-scrollbelowslider" id="slide-972-layer-4"
						data-x="['center','center','center','center']"
						data-hoffset="['0','0','0','0']"
						data-y="['bottom','bottom','bottom','bottom']"
						data-voffset="['40','20','20','20']"
						data-width="['45','45','45','45']"
						data-height="['45','45','45','45']"
						data-whitespace="nowrap"
						data-type="svg"
						data-actions="[{&quot;event&quot;:&quot;click&quot;,&quot;action&quot;:&quot;scrollbelow&quot;,&quot;offset&quot;:&quot;px&quot;,&quot;delay&quot;:&quot;&quot;,&quot;speed&quot;:&quot;1000&quot;,&quot;ease&quot;:&quot;Power1.easeInOut&quot;}]"
						data-svg_src="https://revolution.themepunch.com/wp-content/plugins/revslider/public/assets/assets/svg/hardware/ic_keyboard_arrow_down_24px.svg"
						data-svg_idle="sc:transparent;sw:0;sda:0;sdo:0;"
						data-svg_hover="sc:transparent;sw:0;sda:0;sdo:0;"
						data-basealign="slide"
						data-responsive_offset="off"
						data-responsive="off"
						data-frames="[{&quot;delay&quot;:1500,&quot;speed&quot;:1000,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:bottom;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power4.easeOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;frame&quot;:&quot;hover&quot;,&quot;speed&quot;:&quot;150&quot;,&quot;ease&quot;:&quot;Linear.easeNone&quot;,&quot;to&quot;:&quot;o:1;rX:0;rY:0;rZ:0;z:0;&quot;,&quot;style&quot;:&quot;c:rgb(46,216,163);bc:rgb(46,216,163);&quot;}]"
						data-textalign="['center','center','center','center']"
						data-paddingtop="[8,8,8,8]"
						data-paddingright="[6,6,6,6]"
						data-paddingbottom="[5,5,5,5]"
						data-paddingleft="[6,6,6,6]" style="z-index: 10; background-color: rgb(12, 198, 82); min-width: 70px; max-width: 70px; color: rgb(255, 255, 255); font-family: &quot;Open Sans&quot;; border-color: rgb(12, 198, 82); border-style: solid; border-width: 2px; border-radius: 40px; cursor: pointer; visibility: inherit; transition: none; text-align: center; line-height: 26px; margin: 0px; padding: 20px 18px 5px; letter-spacing: 0px; font-weight: 400; font-size: 16px; white-space: nowrap; min-height: 70px; max-height: 70px; opacity: 1; transform-origin: 50% 50% 0px; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); box-shadow: rgb(153, 153, 153) 0px 0px 0px 0px;" >
					</div>
               	</li>
	        </ul>
	 		<!-- Example Progress Bar, with a height and background color added to its style attribute -->
	 		<div class="tp-bannertimer" style="height: 5px; background-color: rgba(0, 0, 0, 0.25);"></div>
	    </div>

	</div>
</section>

<!--Header Banner-->

<div>
	<section>
		<div class="nav-listing">
		<div class="container ">
			<nav class="navbar navbar-inverse bg-inverse nav-listing-bar grow">
				<li class="nav-item one "><a class="nav-link" href="#"><img src="img/fav_1.ico"></a></li>
				<li class="nav-item one "><a class="nav-link" href="#"><img src="img/fav_2.ico"></a></li>
				<li class="nav-item one "><a class="nav-link" href="#"><img src="img/fav_3.ico"></a></li>
				<li class="nav-item one "><a class="nav-link" href="#"><img src="img/fav_4.ico"></a></li>
				<li class="nav-item one "><a class="nav-link" href="#"><img src="img/fav_5.ico"></a></li>
				<li class="nav-item one "><a class="nav-link" href="#"><img src="img/fav_6.ico"></a></li>
			</nav>
		</div>
		</div>
	
	</section>
</div>

<!--News tricker-->
		<div class="content">
			<div class="simple-marquee-container">
				<div class="marquee-sibling">
					News Feed
				</div>
				<div class="marquee">
					<ul class="marquee-content-items">
						<li>How to become forex signal provider</li>
						<li>Best Forex trading signal providers in 2018</li>
						<li>Making money in forex is easy if you know how the bankers trade</li>
						<li>12 Things I Learned By Being A Forex Trader</li>
						<li>Can You Really Become a Millionaire from Forex Trading ?</li>
					</ul>
				</div>
			</div>
		</div>



<!-- FEATURE -->
<section class="home-feature">
	<div class="container">

		<div class="row justify-content-md-center">
			<div class="col-xl-5 col-lg-6 col-md-8">
				<div class="section-title text-center title-ex1">
					<h2>What we do</h2>
					<p>Clients are at the center of everything we do. </p>
				</div>
			</div>
		</div>
		<div class="service-ex1">
			<div class="row">
				<div class="col-lg-4">
					<!-- single info block -->
					<div class="info text-center">
						<div class="icon icon-circle">
							<i class="icon-pencil"></i>
						</div>
						<h5 class="info-title">Web Design & Development</h5>
						<div class="description">
							<p class="description">We provides affordable web design & web development services with expert customer support. 
							We meet project deadline very well and fulfill client’s requirement within given time.</p>
						</div>
					</div><!-- single info block ends -->
				</div>
				<div class="col-lg-4">
					<!-- single info block -->
					<div class="info text-center">
						<div class="icon icon-circle">
							<i class="icon-layers" aria-hidden="true"></i>
						</div>
						<div class="description">
							<h5 class="info-title">Software & Apps Development</h5>
							<p class="description">Our Development team is specialized in developing multiplatform apps and software. 
							We can also develop custom business software/apps  addressing your specific business process.</p>
						</div>
					</div><!-- single info block ends -->
				</div>
				<div class="col-lg-4">
					<!-- single info block -->
					<div class="info text-center">
						<div class="icon icon-circle">
							<i class="icon-support" aria-hidden="true"></i>
						</div>
						<div class="description">
							<h5 class="info-title">Digital Marketing & Web Hosting</h5>
							<p class="description">We helps business to connect with the digital world. We provides services all according to your desire. 
							Our group of experts is very concern about client satisfaction.</p>
						</div>
					</div><!-- single info block ends -->
				</div>
			</div>
		</div>
	</div>
</section>

<!-- =============================================
# Home about us
=============================================== -->
<section class="bg-sand hero-block home-about">
	<div class="container">
		<div class="row">
			<div class="col-md-6 ">
				<div class="section-title title-ex1">
					<h2 class="title-text">About Us</h2>
				</div>
				<p class="description", align=justify>
					FUTURE TRACK IT is one of the leading IT Company in Bangladesh, has started its journey with an earnest aim to 
					emerge as one the finest Web Development Firms in the world.  We started our company with some most experienced 
					professionals who always dedicated for the company.  
				</p>
				<p class="description", align=justify>
					Our well-equipped resourceful and dedicated team members are 
					the most fundamental asset of Future Track IT, Who never fears to accept challenge and deliver the best. The main 
					motive is to satisfy our customer by providing our level best support and service...
				</p>
				<a href="page-aboutus.html" class="btn btn-default btn-primary">Read More</a>
			</div>

			<div class="col-md-6 ">
				<div class="promo-video bg-image" style="background-image: url('img/about-promo.jpg');">
					<div class="video-button video-box">
						<a href="javascript:void(0)">
							<i class="fa fa-play play-icon" aria-hidden="true" data-video="https://www.youtube.com/embed/rdFgyU5xBpk"></i>
							<span class="hide">Watch Video</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- OUR PORTFOLIO -->
<section class="home-services">
	<div class="container">
		<!-- section title -->
		<div class="row justify-content-md-center">
			<div class="col-xl-5 col-lg-6 col-md-8">
				<div class="section-title title-ex1 text-center">
					<h2 class="title-text">Our Services</h2>
					<p class="description">Our aim is to provide a high quality service which satisfaction of clients.</p>
				</div>
			</div>
		</div>
		<!-- section title ends -->
		<div class="row">
			<div class="col-md-6 col-lg-4">
				<!-- single info block -->
				<div class="info text-center info-boxed">
					<div class="icon icon-circle">
						<i class="icon-bulb" aria-hidden="true"></i>
					</div>
					<div class="description">
						<h5 class="info-title">Web Development</h5>
						<p class="description">We provide a variety of website design and development services, 
						from creating mobile web development solutions and responsive website..</p>
						<a href="service_responsive_webdesign.html">Read More</a>
					</div>
				</div><!-- single info block ends -->
			</div>
			<div class="col-md-6 col-lg-4">
				<!-- single info block -->
				<div class="info text-center info-boxed">
					<div class="icon icon-circle">
						<i class="icon-key" aria-hidden="true"></i>
					</div>
					<div class="description">
						<h5 class="info-title">Software Development</h5>
						<p class="description">Our Development team is specialized in developing multi-platform software. 
						We can also develop custom business software addressing your specific..</p>
						<a href="service_erp.html">Read More</a>
					</div>
				</div><!-- single info block ends -->
			</div>
			<div class="col-md-6 col-lg-4">
				<!-- single info block -->
				<div class="info text-center info-boxed">
					<div class="icon icon-circle">
						<i class="icon-pencil" aria-hidden="true"></i>
					</div>
					<div class="description">
						<h5 class="info-title">Apps Development</h5>
						<p class="description">Along with full-cycle development we can build apps on 
						all the major platforms including iOS, Android, BlackBerry OS and Windows Mobile..</p>
						<a href="service_ios.html">Read More</a>
					</div>
				</div><!-- single info block ends -->
			</div>
			<div class="col-md-6 col-lg-4">
				<!-- single info block -->
				<div class="info text-center info-boxed">
					<div class="icon icon-circle">
						<i class="icon-support" aria-hidden="true"></i>
					</div>
					<div class="description">
						<h5 class="info-title">Digital Marketing</h5>
						<p class="description">We helps business to connect with the digital world. 
						We provides services all according to your desire. Our group of experts is very concern..</p>
						<a href="service_email.html">Read More</a>
					</div>
				</div><!-- single info block ends -->
			</div>
			<div class="col-md-6 col-lg-4">
				<!-- single info block -->
				<div class="info text-center info-boxed">
					<div class="icon icon-circle">
						<i class="icon-screen-desktop" aria-hidden="true"></i>
					</div>
					<div class="description">
						<h5 class="info-title">Web Hosting</h5>
						<p class="description">You will get any kind of domain hosting service within a reasonable price. 
						We provide different types of web hosting service such as professional web.. </p>
						<a href="service_shared.html">Read More</a>
					</div>
				</div><!-- single info block ends -->
			</div>
			<div class="col-md-6 col-lg-4">
				<!-- single info block -->
				<div class="info text-center info-boxed">
					<div class="icon icon-circle">
						<i class="icon-camrecorder" aria-hidden="true"></i>
					</div>
					<div class="description">
						<h5 class="info-title">Graphic Design</h5>
						<p class="description">Our professional and skilled graphic designers are here to create your vision. 
						We offer clean, clear and unique designs with your business image..</p>
						<a href="service_logo.html">Read More</a>
					</div>
				</div><!-- single info block ends -->
			</div>
		</div>
	</div>
</section>

<!-- COUNTING UP -->
<section class="countup-section countUp counter2 bg-sand" style="background-image:url(img/scores.jpg)">	
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-lg-3">
				<!-- single info counter -->
				<div class="info text-center info-count info-boxed">
					<div class="icon icon-circle">
						<i class="fa fa-check-square-o"  aria-hidden="true"></i>
					</div>
					<div class="count_block">
						<div class="counter">234</div>
						<p class="count-text">Total Projects</p>
					</div>
				</div><!-- single info counter ends -->
			</div>
			<div class="col-md-6 col-lg-3">
				<!-- single info counter -->
				<div class="info text-center info-count info-boxed">
					<div class="icon icon-circle">
						<i class="fa fa-users" aria-hidden="true"></i>
					</div>
					<div class="count_block">
						<div class="counter">21547</div>
						<p class="count-text">Working Hours</p>
					</div>
				</div><!-- single info counter ends -->
			</div>
			<div class="col-md-6 col-lg-3">
				<!-- single info counter -->
				<div class="info text-center info-count info-boxed">
					<div class="icon icon-circle">
						<i class="icon-cup" aria-hidden="true"></i>
					</div>
					<div class="count_block">
						<div class="counter">181</div>
						<p class="count-text">Total Clients</p>
					</div>
				</div><!-- single info counter ends -->
			</div>
			<div class="col-md-6 col-lg-3">
				<!-- single info counter -->
				<div class="info text-center info-count info-boxed">
					<div class="icon icon-circle">
						<i class="icon-user" aria-hidden="true"></i>
					</div>
					<div class="count_block">
						<div class="counter">7</div>
						<p class="count-text">Regional Office</p>
					</div>
				</div><!-- single info counter ends -->
			</div>
		</div>
	</div>
</section>
<!-- SKILLS & FAQS-->
<section class="about-more">
	<div class="container">
		<div class="row">
			<div class="col-md-6 skill">
				<div class="row">
					<div class="col-md-11">
						<div class="section-title title-ex1">
							<h2 class="title-text">Our skills</h2>
							<p class="description">Our team provide an amazing array of skills, experience and expertise.</p>
						</div>
					</div>
				</div>
				<!-- progressbar starts -->
				<div class="progress-holder">
					<div class="barWrapper">
						<span class="progressText"><B>Software & Apps Development</B></span>
						<div class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>  
					        <span  class="popOver" data-toggle="tooltip" data-placement="top" title="80%"> </span>     
						</div>
					</div>
					<div class="barWrapper">
						<span class="progressText"><B>Web Design & Development</B></span>
						<div class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>  
					        <span  class="popOver" data-toggle="tooltip" data-placement="top" title="95%"> </span>     
						</div>
					</div>
					<div class="barWrapper">
						<span class="progressText"><B>Digital Marketing</B></span>
						<div class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>  
					        <span  class="popOver" data-toggle="tooltip" data-placement="top" title="85%"> </span>     
						</div>
					</div>
					<div class="barWrapper">
						<span class="progressText"><B>Graphics Design</B></span>
						<div class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>  
					        <span  class="popOver" data-toggle="tooltip" data-placement="top" title="75%"> </span>     
						</div>
					</div>
				</div>
				<!-- progressbar ends -->
			</div>

			<div class="col-md-6 faq">
				<div class="row">
					<div class="col-md-11">
						<div class="section-title title-ex1">
							<h2 class="title-text">FAQ</h2>
							<p class="description">Few facts about our firm that might peak your interest..</p>
						</div>
					</div>
				</div>
				<!-- collapse starts-->
				<div id="accordion" class="th-accordions" data-children=".item">
					<div class="item">
						<a data-toggle="collapse" class="collapse-bar" data-parent="#accordion" href="#accordion1" aria-expanded="true" aria-controls="accordion1">
							<i class="fa fa-database" aria-hidden="true"></i> What is our technical stack?
						</a>
						<div id="accordion1" class="collapse show tab-pane" role="tabpanel">
							<p align=justify>
								We mainly work on laravel framework, python django framework core java script and libraries to meet software/website needs.
							</p>
						</div>
					</div>
					<div class="item">
						<a data-toggle="collapse" class="collapse-bar" data-parent="#accordion" href="#accordion2" aria-expanded="false" aria-controls="accordion2">
							<i class="fa fa-desktop" aria-hidden="true"></i> Is there any methodology we follow?
						</a>
						<div id="accordion2" class="collapse tab-pane" role="tabpanel">
							<p align=justify>
								There are some hybrid methodologies we follow by mixing up with agile.
							</p>
						</div>
					</div>
					<div class="item">
						<a data-toggle="collapse" class="collapse-bar" data-parent="#accordion" href="#accordion3" aria-expanded="false" aria-controls="accordion3">
							 <i class="fa fa-empire" aria-hidden="true"></i> What do we use to maintain project timeline and programs?
						</a>
						<div id="accordion3" class="collapse tab-pane" role="tabpanel">
							<p align=justify>
								For maintaining project timeline and programs We usually maintain gantt chart including some project management tools by following scrum.
							</p>
						</div>
					</div>
					<div class="item">
					<a data-toggle="collapse" class="collapse-bar" data-parent="#accordion" href="#accordion4" aria-expanded="false" aria-controls="accordion4">
							<i class="fa fa-life-ring" aria-hidden="true"></i> What if I have other questions? Who do I contac?
						</a>
						<div id="accordion4" class="collapse tab-pane" role="tabpanel">
							<p align=justify>
								Please call  at +88 01733 475 968/+88 01675 583 178 or email info@ftit.co.uk. Our support is available during normal business hours from 12 a.m. to 8 p.m., Monday through Saturday to respond to inquiries. We will return calls within 24 hours of receipt during the business week and provide after-hours voicemail services for calls outside of normal business hours. If you reach voicemail, we are helping other clients and will return your response shortly.
							</p>
						</div>
					</div>
				</div>
				<!-- collapse ends -->
			</div>
		</div>
	</div>
</section>
<!-- OUR PORTFOLIO -->
<section class="home-portfolio">
	<div class="container ">
		<!-- section title -->
		<div class="row justify-content-md-center">
			<div class="col-xl-5 col-lg-6 col-md-8">
				<div class="section-title text-center title-ex1">
					<h2>Our Portfolio</h2>
					<p>We enhance your recognition and originality with versatile ideas for every sphere of the digital world!</p>
				</div>
			</div>
		</div>
		<!-- section title ends -->
	</div>
	<div class=" container-fluid ">
		<div class="row">
			<div class="catagory col-12 justify-content-center">
				<ul id="filters">
					<li class="button is-checked active" data-filter=".all">
						<a href="">See All</a>
					</li>
					<li class="button " data-filter=".ui-design">
						<a href="">Web & Software Development</a>
					</li>
					<li class="button " data-filter=".branding">
						<a href="">Apps Development</a>
					</li>
					<li class="button " data-filter=".photography">
						<a href="">Digital Marketing & Web Hosting</a>
					</li>
					<li class="button " data-filter=".web-design">
						<a href="">Graphics Design</a>
					</li>
				</ul>
			</div>
		</div>
		<!-- gallery details row -->
		<div class="row no-gutters portfolio_grid">
			<div class="col-lg-3 col-sm-6 element all branding">
				<div class="card">
					<div class="card_img">
						<a href="">
						    <img class="img-full" src="img/portfolio/p1.jpg" />
							<div class="hover-overlay effect-scale">
								<div class="overlay-content">
									<a href="portfolio_artworks.html" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
									<a href="#" data-fancybox="images" class="icon icon-circle">
										<i class="fa fa-search" aria-hidden="true"></i>
									</a>
							    	<a href=""><h6 class="overlay-title">Artworks Graphic Design LTD</h6></a>
							    	<span>Apps Development</span>
								</div>
						    </div>
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6 element all ui-design">
				<div class="card">
					<div class="card_img">
						<a href="">
						    <img class="img-full" src="img/portfolio/p2.jpg" />
							<div class="hover-overlay effect-scale">
								<div class="overlay-content">
									<a href="portfolio_spell.html" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
									<a href="img/portfolio/p2.jpg" data-fancybox="images" class="icon icon-circle">
										<i class="fa fa-search" aria-hidden="true"></i>
									</a>
							    	<a href=""><h6 class="overlay-title">Spell Net IT</h6></a>
							    	<span>Web & Software Development</span>
								</div>
						    </div>
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6  element all web-design">
				<div class="card">
					<div class="card_img">
						<a href="">
						    <img class="img-full" src="img/portfolio/p3.jpg" />
							<div class="hover-overlay effect-scale">
								<div class="overlay-content">
									<a href="portfolio_premera.html" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
									<a href="img/portfolio/p3.jpg" data-fancybox="images" class="icon icon-circle">
										<i class="fa fa-search" aria-hidden="true"></i>
									</a>
							    	<a href=""><h6 class="overlay-title">Premera Support Agency</h6></a>
							    	<span>Graphics Design</span>
								</div>
						    </div>
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6  element all ui-design web-design">
				<div class="card">
					<div class="card_img">
						<a href="">
						    <img class="img-full" src="img/portfolio/p4.jpg" />
							<div class="hover-overlay effect-scale">
								<div class="overlay-content">
									<a href="portfolio_trusty.html" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
									<a href="img/portfolio/p4.jpg" data-fancybox="images" class="icon icon-circle">
										<i class="fa fa-search" aria-hidden="true"></i>
									</a>
							    	<a href=""><h6 class="overlay-title">Trusty Consultancies</h6></a>
							    	<span>Web & Software Development</span>
								</div>
						    </div>
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6  element all photography branding">
				<div class="card">
					<div class="card_img">
						<a href="">
						    <img class="img-full" src="img/portfolio/p5.jpg" />
							<div class="hover-overlay effect-scale">
								<div class="overlay-content">
									<a href="portfolio_m2m.html" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
									<a href="img/portfolio/p5.jpg" data-fancybox="images" class="icon icon-circle">
										<i class="fa fa-search" aria-hidden="true"></i>
									</a>
							    	<a href=""><h6 class="overlay-title">M2M Communication</h6></a>
							    	<span>Apps Development</span>
								</div>
						    </div>
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6  element all ui-design web-design">
				<div class="card">
					<div class="card_img">
						<a href="">
						    <img class="img-full" src="img/portfolio/p6.jpg" />
							<div class="hover-overlay effect-scale">
								<div class="overlay-content">
									<a href="portfolio_spectrum.html" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
									<a href="img/portfolio/p6.jpg" data-fancybox="images" class="icon icon-circle">
										<i class="fa fa-search" aria-hidden="true"></i>
									</a>
							    	<a href=""><h6 class="overlay-title">Spectrum Advertising Limited</h6></a>
							    	<span>Web & Software Development</span>
								</div>
						    </div>
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6  element all photography branding">
				<div class="card">
					<div class="card_img">
						<a href="">
						    <img class="img-full" src="img/portfolio/p7.jpg" />
							<div class="hover-overlay effect-scale">
								<div class="overlay-content">
									<a href="portfolio_uoda.html" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
									<a href="img/portfolio/p7.jpg" data-fancybox="images" class="icon icon-circle">
										<i class="fa fa-search" aria-hidden="true"></i>
									</a>
							    	<a href=""><h6 class="overlay-title">UODA (University)</h6></a>
							    	<span>Apps Development</span>
								</div>
						    </div>
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-sm-6  element all ui-design branding photography">
				<div class="card">
					<div class="card_img">
						<a href="">
						    <img class="img-full" src="img/portfolio/p8.jpg" />
							<div class="hover-overlay effect-scale">
								<div class="overlay-content">
									<a href="portfolio_sushiyan.html" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
									<a href="img/portfolio/p8.jpg" data-fancybox="images" class="icon icon-circle">
										<i class="fa fa-search" aria-hidden="true"></i>
									</a>
							    	<a href=""><h6 class="overlay-title">Sushiyan IT</h6></a>
							    	<span>Web & Software Development</span>
								</div>
						    </div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<!-- Filter row ends -->
	</div>
</section>

<!-- OUR TEAM -->
<section class="home-team">
	<div class="container">
		<!-- section title -->
		<div class="row justify-content-md-center">
			<div class="col-xl-5 col-lg-6 col-md-8">
				<div class="section-title text-center title-ex1">
					<h2>Meet Our Team</h2>
					<p>Our team, your needs, One goal..</p>
				</div>
			</div>
		</div>
		<!-- section title ends -->
		<div id="card" class="row">
			<div class="col-md-6 col-lg-3">
				<div class="card team-card">
					<div class="card_img">
						<img class="img-full" src="img/about/team1.jpg" alt="Card image">
						<div class="hover-overlay effect-scale">
							<div class="overlay-content">
								<a href="#" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
							</div>
						</div>
					</div>
					<div class="card-block">
						<h6 class="card-title">RAJ SAHA</h6>
						<span class="">CEO</span>
					</div>
				</div>
			</div>
			
			<div class="col-md-6 col-lg-3">
				<div class="card team-card">
					<div class="card_img">
						<img class="card-img-top img-full" src="img/about/team2.jpg" alt="Card image">
						<div class="hover-overlay effect-scale">
							<div class="overlay-content">
								<a href="#" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
							</div>
						</div>
					</div>
					<div class="card-block">
						<h6 class="card-title">KAZI ABDULLAH ARAFAT</h6>
						<span class="">HR ADMIN</span>
					</div>
				</div>
			</div>
			
			<div class="col-md-6 col-lg-3">
				<div class="card team-card">
					<div class="card_img">
						<img class="img-full" src="img/about/team3.jpg" alt="Card image">
						<div class="hover-overlay effect-scale">
							<div class="overlay-content">
							<a href="#" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
							</div>
						</div>
					</div>
					<div class="card-block">
						<h6 class="card-title">FAKHAR UDDIN (ARNOB)</h6>
						<span class="">HEAD OF IT</span>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-3">
				<div class="card team-card">
					<div class="card_img">
						<img class="img-full" src="img/about/team4.jpg" alt="Card image">
						<div class="hover-overlay effect-scale">
							<div class="overlay-content">
								<a href="#" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
							</div>
						</div>
					</div>
					<div class="card-block">
						<h6 class="card-title">AB SIDDIQUE SHOHEL</h6>
						<span class="">IT CONSULTANT</span>
					</div>
				</div>
			</div>
			
			<div class="col-md-6 col-lg-3">
				<div class="card team-card">
					<div class="card_img">
						<img class="img-full" src="img/about/team5.jpg" alt="Card image">
						<div class="hover-overlay effect-scale">
							<div class="overlay-content">
								<a href="#" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
							</div>
						</div>
					</div>
					<div class="card-block">
						<h6 class="card-title">SHEIKH MINHAZ UDDIN</h6>
						<span class="">SOFTWARE ENGINEER</span>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-3">
				<div class="card team-card">
					<div class="card_img">
						<img class="img-full" src="img/about/team6.jpg" alt="Card image">
						<div class="hover-overlay effect-scale">
							<div class="overlay-content">
								<a href="#" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
							</div>
						</div>
					</div>
					<div class="card-block">
						<h6 class="card-title">AL MAMUN KHAN</h6>
						<span class="">SOFTWARE ENGINEER</span>
					</div>
				</div>
			</div>
			
			<div class="col-md-6 col-lg-3">
				<div class="card team-card">
					<div class="card_img">
						<img class="img-full" src="img/about/team7.jpg" alt="Card image">
						<div class="hover-overlay effect-scale">
							<div class="overlay-content">
								<a href="#" class="icon icon-circle">
										<i class="fa fa-link" aria-hidden="true"></i>
									</a>
							</div>
						</div>
					</div>
					<div class="card-block">
						<h6 class="card-title">REJOANUL FERDOUSH</h6>
						<span class="">FRONT END DESIGNER</span>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
<!-- TESTIMONIAL SECTION -->
<div class="home-testimonial  testimonial2 bg-image" style="background-image: url('img/bg-testimonial2.jpg'); ">
	
		<div class="container">
			<div id="home-testimonial" class="kit-testimonial-carousel">
				<div class="item row flex-md-row-reverse justify-content-between">
					<div class="text-center text-md-right right-column col-md-4">
						<img src="img/client2.png" alt="testimonial image">
					</div>
					<!-- Right column -->
					<div class="left-column col-md-8">
						<div class="card ">
							<div class="card-block">
								<i class="fa fa-quote-right"></i>
								<p class="pr-4">Working with "FUTURE TRACK IT" inspired confidence and brought efficiency to our "SPELLNET IT" project. 
								Their team was very knowledgeable and helpful, worked quickly and effectively while clearly communicating, and completed 
								a painless migration early and under budget.</p>
								
								<h6 class="card-title">ALEX JONES</h6>
								<span class="">FOUNDER</span>
							</div>
						</div>
					</div>
				</div>
			<div class="item row flex-md-row-reverse justify-content-between">
				<div class="text-center text-md-right right-column col-md-4">
					<img src="img/client2.png" alt="testimonial image">
				</div>
				<!-- Right column -->
				<div class="left-column col-md-8">
					<div class="card ">
						<div class="card-block">
							<i class="fa fa-quote-right"></i>
							<p class="pr-4">Our Company "ARCTURUS TECHNOLOGY" need to constantly update our website and provide our clients and job applicants with quality content. 
							We selected to hire "FUTURE TRACK IT" in order to achieve this goal and provide an engaging and compelling experience to our website users.</p>
							
							<h6 class="card-title">MIZHBAH UDDIN</h6>
							<span class="">CEO</span>
						</div>
					</div>
				</div>
			</div>
			<div class="item row flex-md-row-reverse justify-content-between">
				<div class="text-center text-md-right right-column col-md-4">
					<img src="img/client2.png" alt="testimonial image">
				</div>
				<!-- Right column -->
				<div class="left-column col-md-8">
					<div class="card ">
						<div class="card-block">
							<i class="fa fa-quote-right"></i>
							<p class="pr-4">"FUTURE TRACK IT" is a highly skilled and uniquely capable firm with multitudes of talent on-board. 
							Our company "SM TRADE" have collaborated on a number of diverse projects over the years all of which have been utmost success for both us and our clients. 
							We are working to expand our collaboration with them and highly recommend them to others as well.</p>
							
							<h6 class="card-title">ZUBAIR AHMAAD</h6>
							<span class="">FOUNDER</span>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<!-- FEATURE -->
<section class="pricing-section">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-xl-5 col-lg-6 col-md-8">
				<div class="section-title text-center title-ex1">
					<h2>Pricing Tables</h2>
					<p>Choose the package most suitable for you and quote us..</p>
				</div>
			</div>
		</div>
		<!-- Pricing Table starts -->
		<div class="row">
			<div class="col-md-4">
				<div class="price-card ">
					<h2>STARTER</h2>
					<p>Semi-Custom Website Design</p>
					<ul class="pricing-offers">
					    <br>
						<li>One Design Concept + One Revision</li>
						<li>Responsive Design</li>
						<li>Up to 10 Website Pages</li>
						<li>Basic Wordpress Development</li>
						<li>1 E-mail Contact Form</li>
						<li>Photo Gallery Support</li>
						<li>Home Page Slideshow (2 pictures)</li>
						<li>Primary Website Training</li>
						<li>User Instructions</li>
						<li>Other Features – Quote</li>
					</ul>
					<a href="#" class="btn btn-inverse btn-mid">QUOTE US</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="price-card featured">
					<h2>STANDARD</h2>
					<p>Custom Website Design</p>
					<ul class="pricing-offers">
					    <br>
						<li>One Design Concept + Two Revisions</li>
						<li>Responsive Design</li>
						<li>Up to 20 Website Pages Developed</li>
						<li>Wordpress Development</li>
						<li>1 E-mail Contact Form</li>
						<li>Home Page Slideshow (6 pictures)</li>
						<li>Photo Gallery Support</li>
						<li>Google Analytics Setup</li>
						<li>In-Person (Local) Website Training</li>
						<li>Other Features – Quote</li>
					</ul>
					<a href="#" class="btn btn-inverse btn-mid">QUOTE US</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="price-card ">
					<h2>PROFESSIONAL</h2>
					<p>Custom Website Design</p>
					<br>
					<ul class="pricing-offers">
						<li>One Design Concept + Two Revisions</li>
						<li>Responsive Design</li>
						<li>Up to 20 Website Pages Developed</li>
						<li>Wordpress Development</li>
						<li>2 E-mail Contact Form</li>
						<li>Event Calendar</li>
						<li>Paypal Shopping Cart Option</li>
						<li>Google Analytics Setup</li>
						<li>10 Phrase Keyword Research</li>
						<li>Other Features – Quote</li>
					</ul>
					<a href="#" class="btn btn-inverse btn-mid">QUOTE US</a>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Carousel Middle -->

	<section class="saleofweeek">
        <div class="container">
            <div class="sec-title ltproject">
                <h2>Latest Projects</h2>
            </div>

            <div class="sowbox">
                <div class="switem">
                    <a href="#">
                        <img src="img/c1.jpg" />
                        <div class="swhoverbox">
                            <h3>Business Growth</h3>
                        </div>
                    </a>
                </div>
                <div class="switem">
                    <a href="#">
                        <img src="img/c1.jpg" />                            
                        <div class="swhoverbox">
                            <h3>Startup Business</h3>
                        </div>
                    </a>
                </div>
                <div class="switem">
                    <a href="#">
                        <img src="img/c1.jpg" />
                        <div class="swhoverbox">
                            <h3>Financial Service</h3>
                        </div>
                    </a>
                </div>
                <div class="switem">
                    <a href="#">
                        <img src="img/c1.jpg" />
                        <div class="swhoverbox">
                            <h3>Exer Saucer Exhibitor</h3>
                        </div>
                    </a>
                </div>
                <div class="switem">
                    <a href="#">
                        <img src="img/c1.jpg" />
                        <div class="swhoverbox">
                            <h3>Estate planning</h3>
                        </div>
                    </a>
                </div>
                <div class="switem">
                    <a href="#">
                        <img src="img/c1.jpg" />
                        <div class="swhoverbox">
                            <h3>Business Planning</h3>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

<!-- TESTIMONIAL SECTION -->
<section class="contact">
	<div class="container">
		<div class="row ">
			<div class="col-md-7 col-lg-5 ">
				<div class="section-title title-ex1">
					<h2 class="">Say Hello!</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<!-- Left column -->
			<div class="col-md-5 col-lg-4">
				<div class="media contact-media">
					<div class="icon icon-circle">
						<i class="fa fa-map-marker" aria-hidden="true"></i>
					</div>
					<div class="media-body">
						965/1-A, Shewrapara, Salam Abad Complex Mirpur, Dhaka-1216.
					</div>
				</div>
				<div class="media contact-media">
					<div class="icon icon-circle">
						<i class="fa fa-phone" aria-hidden="true"></i>
					</div>
					<div class="media-body">
						+88 01733 475 968 <br>
                        +88 01675 583 178
					</div>
				</div>
				<div class="media contact-media">
					<div class="icon icon-circle">
						<i class="fa fa-envelope-o" aria-hidden="true"></i>
					</div>
					<div class="media-body">
						info@ftit.co.uk <br>
						ceo@ftit.co.uk
					</div>
				</div>
			</div><!-- Left column -->

			<!-- Right column -->
			<div class="col-md-7 col-lg-8">
				<!-- Contact Form Starts-->
				<form class="">
					<div class="row ">
						<div class="form-group col-md-6">
						  <input type="text" class="form-control" id="exampleInputName" placeholder="Name">
						</div>
						<div class="form-group col-md-6">
						  <input type="email" class="form-control" id="exampleInputEmail" placeholder="Email">
						</div>
						<div class="form-group col-md-12">
						   <textarea class="form-control" id="exampleTextarea" rows="6" placeholder="Message"></textarea>
						</div>
					</div>
					<div class="d-flex">
				  		<button type="submit" class="btn btn-lg btn-primary ml-auto">Send Message</button>
					</div>
				</form><!-- Contact Form ends-->
			</div><!-- Right column -->
		</div>
	</div>
</section>

<!-- FOOTER -->
			<footer class="footer footer-classic bg-dark">
				<div class="container">
				<div class="row">
		<!-- About -->
		<div class="col-md-3 md-margin-bottom-40 box_down">
			<a href="index.html"><img id="logo-footer" class="footer-logo" src="img/logo.png" alt="web design company in dhaka bangladesh"></a>
					<p align=justify>FUTURE TRACK IT is one of the leading IT Company in Bangladesh, has started its journey 
					with an earnest aim to emerge as one the finest Web Development Firms in the world. 
					We started our company  <a href="page-aboutus.html"><span class="read-more">Read more</span></a><!-- read-more class in tooltip.css -->
					<br/> </p>  
					</div><!--/col-md-3-->
					<div class="col-md-3 md-margin-bottom-40">
					<div class="posts">
					<div class="foot-headline">Our Services</div>
					<ul class="list-unstyled link-list">
					<li > </i><a href="service_responsive_webdesign.html">Responsive Website Design</a></li>
					<li> <a href="service_e-commerce.html">E-Commerce Websites</a></li>
					<li> <a href="service_content_management.html">CMS Development</a></li>
					<li> <a href="service_event_booking.html">Event Booking System</a></li>
					<li> <a href="service_website_repairs.html">Website Repairs & Maintenance</a></li>
					<li> <a href="service_email.html">Email Newsletter</a></li>
					<li> <a href="service_networking.html">Social Networking</a></li>
					<li> <a href="service_sem.html">Search Engine Marketing (SEM)</a></li>
					<li> <a href="service_seo.html">Search Engine Optimization (SEO)</a></li>
				</ul>
			</div>
		</div><!--/col-md-3--> 
<!-- Link List -->
		<div class="col-md-3 md-margin-bottom-40">
			<div class="foot-headline">Our Services </div>
				<ul class="list-unstyled link-list">
				<li><a href="service_on_off.html"title="Software Development">On-site & Off-site Marketing</a></li>
				<li><a href="service_shared.html"title="Web Design Company in Bangladesh">Shared Web Hosting</a></li>
				<li><a href="service_dedicated.html"title="Web Development">Dedicated Web Server</a></li>
				<li><a href="service_reseller.html"title="E-Commerce Solutions">Reseller Web Hosting</a></li>
				<li><a href="service_cloud.html">Cloud Web Hosting</a></li>
				<li><a href="service_virtual.html"title="Domain">Virtual Private Server(VPS)</a></li>
				<li><a href="service_colocation.html"title="Hosting">Collocation Web Hosting</a> </li>
				<li><a href="service_management.html"title="Hosting">Management Software DEV</a></li>
				<li><a href="service_automation.html"title="Hosting">Office Automation SW DEV</a></li>
			</ul>
		</div><!--/col-md-3-->
<!-- End Link List -->                    

<!-- Address -->
		<div class="col-md-3 ">
			<div class="foot-headline ">Contact Us</div> 
				<ul class="list-unstyled link-list">
				
				<li><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					United Kingdom</a>
					<div class="collapse" id="collapseOne">
					<div class="collapse show tab-pane" style="color:#fff">
					<div class="collapse show tab-pane">
								Bullands Close Bovey Tracey Devon TQ13 9JF, United Kingdom.
					</div>
					</div>
					</div></li>

				<li><a data-toggle="collapse" data-parent="#accordion" href="#collapseExample1" aria-expanded="true" aria-controls="collapseOne">
					United Arab Emirates</a>
					<div class="collapse" id="collapseExample1">
					<div class="collapse show tab-pane" style="color:#fff">
					<div class="collapse show tab-pane">
								FZE Business Center-4 (8th Floor), P.O.Box 325 782 Ras Al Khaimah, United Arab Emirates.<br>
								Phone : +442033180427
					</div>
					</div></li>

				<li><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
					UK</a>
					<div class="collapse" id="collapseOne1">
					<div class="collapse show tab-pane">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
					</div>
					</div></li>

				<li><a data-toggle="collapse" data-parent="#accordion" href="#collapseExample11" aria-expanded="true" aria-controls="collapseOne">
					Dubai</a>
					<div class="collapse" id="collapseExample11">
					<div class="collapse show tab-pane">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
					</div>
					</div></li>

				<li><a data-toggle="collapse" data-parent="#accordion" href="#collapseExample12" aria-expanded="true" aria-controls="collapseOne">
					Spain</a>
					<div class="collapse" id="collapseExample12">
					<div class="collapse show tab-pane">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
					</div>
					</div></li>

				<li><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne3" aria-expanded="true" aria-controls="collapseOne">
					China</a>

					<div class="collapse" id="collapseOne3">
							<div class="collapse show tab-pane">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
					</div>
					</div></li>

				<li><a data-toggle="collapse" data-parent="#accordion" href="#collapseExample14" aria-expanded="true" aria-controls="collapseOne">
					Korea</a>
					<div class="collapse" id="collapseExample14">
					<div class="collapse show tab-pane">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
					</div>
					</div></li>
		</ul>
	</div><!--/col-md-3-->
</div>
	<!-- COPY RIGHT -->
		<div class="copyright">
			<hr>
				<div class="row justify-content-center">
					<div class="col-md-8 col-lg-6 ">
						<div class="social-icons text-center">
							<a href="#" class="btn btn-social btn-social-o facebook">
							<i class="fa fa-facebook-f"></i>
							</a>
							<a href="#" class="btn btn-social btn-social-o twitter">
							<i class="fa fa-twitter"></i>
							</a>
							<a href="#" class="btn btn-social btn-social-o pinterest">
							<i class="fa fa-pinterest-p"></i>
							</a>
							<a href="#" class="btn btn-social btn-social-o google-plus">
							<i class="fa fa-google-plus"></i>
							</a>
							<a href="#" class="btn btn-social btn-social-o linkedin">
							<i class="fa fa-linkedin"></i>
							</a>
							<a href="#" class="btn btn-social btn-social-o vimeo">
							<i class="fa fa-vimeo"></i>
							</a>
							<a href="#" class="btn btn-social btn-social-o dribbble">
							<i class="fa fa-dribbble" aria-hidden="true"></i>
							</a>
						<div class="copyRight_text text-center">
					<p> © Copyright 2018 Future Track IT All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</footer>
	<a href="#pageTop" class="backToTop"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- Modal -->
<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span class="bar-1"></span>
				<span class="bar-2"></span>
		</button>
		<form action="">
			<input type="text" autofocus name="search" placeholder="Search...">
		</form>
	</div>
	</div>
</div>

<!-- JAVASCRIPTS -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsQdSlW4vj5RvXp2_pLnv1s1ErfxjM5_o"></script>
<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/jquery/jquery-migrate-3.0.0.min.js"></script>
<script src="plugins/bootstrap/js/tether.min.js"></script>
<script src="plugins/bootstrap/js/popper.min.js" ></script>
<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/selectbox/jquery.selectbox-0.1.3.min.js"></script>
<script src="plugins/slick/slick.min.js"></script>
<script src="plugins/circle-progress/jquery.appear.js"></script>
<script src="plugins/isotope/isotope.min.js"></script>
<script src="plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="plugins/counterUp/waypoint.js"></script>
<script src="plugins/counterUp/jquery.counterup.js"></script>
<script src="plugins/smoothscroll/SmoothScroll.js"></script>
<script src="plugins/syotimer/jquery.syotimer.min.js"></script>
<script type="text/javascript" src="plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<!-- Alada -->
<script type="text/javascript" src="js/revolution.extension.actions.min.js"></script>
<script src="js/owl.js"></script>
<script src="js/script.js"></script>
<!-- Alada -->

<script src="js/custom.js"></script>

<!-- News Tricker -->

		<script type="text/javascript" src="js/marquee.js"></script>


		<script>
			$(function (){

				$('.simple-marquee-container').SimpleMarquee();
				
			});

		</script>


</body>

</html>

