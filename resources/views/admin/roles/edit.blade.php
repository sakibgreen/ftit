@extends('admin.layouts.master')

@section('title', '| Edit Role')

@section('content')
<div class="content-wrapper">

<div class='col-lg-4 col-lg-offset-4'>
    <h1><i class='fa fa-key'></i> Edit Role: {{$role->name}}</h1>
    <hr>
    <form class="from-horizontal mt-5" action="{{route('roles.update', $role->id)}}" method="post" enctype="multipart/form-data">
        {{method_field('PUT')}}
        {{csrf_field()}}

        <div class="form-group">
            <label for="name">Role Name</label>
            <input type="text" name="name" class="form-control" value="{{$role->name}}">
        </div>

        <h5><b>Assign Permissions</b></h5>
        @foreach ($permissions as $permission)
            @if($r_permission_id == $permission->id)
                <input type="checkbox" name="permissions[]" value="{{$permission->id}}" checked="checked">{{$permission->name}} <br>
            @else
                <input type="checkbox" name="permissions[]" value="{{$permission->id}}">{{$permission->name}} <br>
            @endif
        @endforeach
        <br>
        <button class="btn btn-info btn-block"> Edit Role </button>
    </form>
    
</div>
</div>
@endsection